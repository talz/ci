ARG NODE_VERSION=22

FROM node:$NODE_VERSION-alpine as base

FROM base as builder

ARG CI_REPO=talz/ci
ARG CI_VERSION=main

# install ci
ENV CI_PATH /usr/local/ci
ENV PATH "$PATH:$CI_PATH/bin"
ADD https://gitlab.com/$CI_REPO/-/raw/$CI_VERSION/mod/common/install-alpine install-ci
RUN chmod +x install-ci && ./install-ci && rm install-ci

ADD package.json package-lock.json ./
RUN env UV_USE_IO_URING=0 npm clean-install && npm cache clean --force

ADD . /app
WORKDIR /app
RUN just build

FROM base as prod

WORKDIR /app

# Add Tini
RUN apk add --update --no-cache tini

ENV NODE_ENV production

COPY package.json package-lock.json /app/
RUN env UV_USE_IO_URING=0 npm clean-install && npm cache clean --force

COPY --from=builder /app/build /app/src/

ENTRYPOINT ["tini", "--", "node", "src/main.js"]
