{
  inputs = {
    utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  };
  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let 
        pkgs = nixpkgs.legacyPackages.${system};
        nodejs = pkgs.nodejs_22;
        stdenv = pkgs.stdenv;
      in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            nodejs
            just
            cargo
            kubernetes-helm
          ];
        };

        defaultPackage = stdenv.mkDerivation {
          name = "ci";
          src = ./.;
          buildPhase = ''
            mkdir $out
            cp -r $src/mod $out/mod
            cp $src/justfile $out
            mkdir $out/bin
            ln -s ${pkgs.just}/bin/just $out/bin/just
          '';
        };
      }
    );
}