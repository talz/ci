# Common Ops

_WORK IN PROGRESS_

This project contains various devops scripts that can be imported into other projects.

Supported runners:

- GitLab CI/CD
- GitHub Actions (soon)
- just

## Modules

| Name                            | Description                                                     |
| ------------------------------- | --------------------------------------------------------------- |
| [release](./mod/release/)       | Generate changelogs automatically based on git commits messages |
| [common](./mod/common/)         |                                                                 |
| [docker](./mod/docker/)         | Build docker images                                             |
| [helm](./mod/helm/)             | Build helm charts                                               |
| [javascript](./mod/javascript/) | Build & manage javascript projects                              |
| [python](./mod/python/)         |                                                                 |
| [repo](./mod/repo/)             |                                                                 |
| [rust](./mod/rust/)             |                                                                 |
| [version](./mod/version/)       |                                                                 |

## Getting Started

## Install

### Flake

```sh
nix profile install gitlab:talz/ci/main
```

### Alpine

```sh
apk add curl
curl https://gitlab.com/talz/ci/-/raw/main/mod/common/install-alpine | sh
```
