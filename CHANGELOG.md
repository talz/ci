## [0.5.0](https://gitlab.com/talz/ci/compare/v0.4.3...v0.5.0) (2024-11-21)

### Features

* added docker/push-image script ([679715b](https://gitlab.com/talz/ci/commit/679715b7c1d351a7c0c94947ec770d38e83e951b))
* allow to set the project name ([5761300](https://gitlab.com/talz/ci/commit/5761300b59e5df3f67c026297d69934973ccec44))
* justfile everywhere ([dd3191f](https://gitlab.com/talz/ci/commit/dd3191f91b420198963b4c17928244d325e2c06f))

### Bug Fixes

* bump not working ([ca09a98](https://gitlab.com/talz/ci/commit/ca09a98479a1c41c47bb4a15ba868b21c580ba95))
## [0.5.0](https://gitlab.com/talz/ci/compare/v0.4.3...v0.5.0) (2024-11-21)

### Features

* added docker/push-image script ([679715b](https://gitlab.com/talz/ci/commit/679715b7c1d351a7c0c94947ec770d38e83e951b))
* allow to set the project name ([5761300](https://gitlab.com/talz/ci/commit/5761300b59e5df3f67c026297d69934973ccec44))
* justfile everywhere ([dd3191f](https://gitlab.com/talz/ci/commit/dd3191f91b420198963b4c17928244d325e2c06f))

### Bug Fixes

* bump not working ([a495316](https://gitlab.com/talz/ci/commit/a495316eed4b960289e8d5fa95486a2b4a75d722))
## [0.4.3](https://gitlab.com/talz/ci/compare/v0.4.2...v0.4.3) (2024-06-02)

* updated dependencies

## [0.4.2](https://gitlab.com/talz/ci/compare/v0.4.1...v0.4.2) (2023-12-28)


### Bug Fixes

* run build jobs ([6e7b3d2](https://gitlab.com/talz/ci/commit/6e7b3d2d1e7b5d87b5afe894189906f6776c470f))

## [0.4.1](https://gitlab.com/talz/ci/compare/v0.4.0...v0.4.1) (2023-12-28)


### Bug Fixes

* rules typo ([ef7d2c9](https://gitlab.com/talz/ci/commit/ef7d2c9e2ffca21102f45b2ab27035e6b6906e7f))

## [0.4.0](https://gitlab.com/talz/ci/compare/v0.3.0...v0.4.0) (2023-12-28)


### Features

* moved from yarn to npm ([ff8aa9c](https://gitlab.com/talz/ci/commit/ff8aa9c89b3adf10bd9dd0dfbe7ee4f09a6e2e4b))

## [0.3.0](https://gitlab.com/talz/ci/compare/v0.2.2...v0.3.0) (2023-04-20)


### Features

* allow to override build chart base's before_script ([d00a852](https://gitlab.com/talz/ci/commit/d00a85253dd74d722fca212f3675fd6a0ba44d1d))

## [0.2.2](https://gitlab.com/talz/ci/compare/v0.2.1...v0.2.2) (2023-03-24)


### Bug Fixes

* changes ([fa07996](https://gitlab.com/talz/ci/commit/fa0799693a2166d579b7bbe88c3dc9effb1ba7c5))

## [0.2.1](https://gitlab.com/talz/ci/compare/v0.2.0...v0.2.1) (2023-03-24)


### Bug Fixes

* removed cleanup ([58ec9f4](https://gitlab.com/talz/ci/commit/58ec9f433c58bf6826cb904a0491c450e620307a))

## 0.2.0 (2023-03-24)


### Features

* improvements ([98de36c](https://gitlab.com/talz/ci/commit/98de36c7444f1d09f60d5021efdba1b4333ea824))


### Bug Fixes

* build chart changes ([5043d92](https://gitlab.com/talz/ci/commit/5043d92afb8a90f521e2c32c15e5e35b485acd90))

