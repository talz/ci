# Color - taken from https://stackoverflow.com/a/65814978/1246924
RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
NC='\033[0m' # No Color

function print_red {
    printf "${RED}$@${NC}\n" 1>&2
}

function print_green {
    printf "${GREEN}$@${NC}\n" 1>&2
}

function print_yellow {
    printf "${YELLOW}$@${NC}\n" 1>&2
}